package net.iessanclemente.a13marcosve.test;

import static org.junit.Assert.*;

import org.junit.Test;

import net.iessanclemente.a13marcosve.main.FizzBuzz;

public class FizzBuzzTest {

	@Test
	public void test() {
		assertEquals("Fizz", FizzBuzz.play(3));
		assertEquals("Buzz", FizzBuzz.play(5));
		assertEquals("FizzBuzz", FizzBuzz.play(15));
		assertEquals("1", FizzBuzz.play(1));
		assertEquals("Fizz", FizzBuzz.play(53));
		assertEquals("Buzz", FizzBuzz.play(52));
	}

}
