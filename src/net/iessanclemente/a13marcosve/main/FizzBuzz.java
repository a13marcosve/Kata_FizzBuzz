package net.iessanclemente.a13marcosve.main;

public class FizzBuzz {
	public static void main(String[] args) {
		// write your code here
		for (int i = 1; i <= 100; i++) {
			System.out.println(play(i));
		}
	}

	/**
	 * Metodo que comproba se o numero � divisible entre 3 ou 5.
	 * 
	 * @param num
	 * @return String
	 * @version 2.0
	 */

	public static String play(int num) {
		if (num % 3 == 0 && num % 5 == 0) {
			return "FizzBuzz";
		} else if (num % 3 == 0 || String.valueOf(num).contains("3")) {
			return "Fizz";
		} else if (num % 5 == 0 || String.valueOf(num).contains("5")) {
			return "Buzz";
		}
		return String.valueOf(num);
	}
}
